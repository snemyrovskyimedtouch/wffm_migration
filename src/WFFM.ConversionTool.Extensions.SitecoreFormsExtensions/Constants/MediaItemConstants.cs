﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFFM.ConversionTool.Extensions.SitecoreFormsExtensions.Constants
{
	public class MediaItemConstants
	{
		public const string ExtensionFieldId = "{C06867FE-9A43-4C7D-B739-48780492D06F}";
		public const string MimeTypeFieldId = "{6F47A0A5-9C94-4B48-ABEB-42D38DEF6054}";
		public const string SizeFieldId = "{6954B7C7-2487-423F-8600-436CB3B6DC0E}";
	}
}
